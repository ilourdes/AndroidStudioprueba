package com.example.cliente.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class Pruebaumg extends AppCompatActivity {
        private EditText et1;
        private EditText et2;
        private Button bt1;
        private TextView tv1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pruebaumg);

        et1=(EditText) findViewById(R.id.et1);
        et2=(EditText) findViewById(R.id.et2);
        bt1=(Button) findViewById(R.id.button);
        tv1=(TextView) findViewById(R.id.tv1);

    }
    public void saveName(View view){
        String name=et1.getText().toString();
        tv1.setText(name);
        et1.setText("");
        et2.setText("");
    }
    public void sumar(View view){
        String num1=et1.getText().toString();
        String num2=et2.getText().toString();
        int suma=Integer.parseInt(num1)+Integer.parseInt(num2);

        tv1.setText(String.valueOf(suma));
        et1.setText("");
        et2.setText("");
    }
    public void restar(View view) {
        String num1 = et1.getText().toString();
        String num2 = et2.getText().toString();
        int resta = Integer.parseInt(num1) - Integer.parseInt(num2);

        tv1.setText(String.valueOf(resta));
        et1.setText("");
        et2.setText("");
    }
}
